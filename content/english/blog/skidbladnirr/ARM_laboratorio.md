---
title: "Tutorial: Configuración de laboratorio de ARM"
date: 2023-01-12T00:00:00+00:00
author: skidbladnir
image_webp: images/blog/skidbladnir/ARM_ensamblador/arm_chip.JPG
image: images/blog/skidbladnir/ARM_ensamblador/arm_chip.JPG
description : "Configuración de laboratorio ARM"
categories: ["Material didactico"]
tags: ["ARM", "ensamblador", "explotacion binaria"]
---

# Configuración de Laboratorio de Explotación Binaria ARM

Crearemos un entorno de ARM utilizando el emulador QEMU. QEMU nos permite no solo emular un distinto entorno operativo, sino toda una arquitectura distinta. Lo utilizaremos para crear una máquina de Raspbian, el sistema operativo basado en Debian hecho para las Raspberry Pis, las cuales cuentan con procesadores ARM. Las siguientes instrucciones deben realizarse en un host con sistema operativo Linux, o en su defecto una máquina virtual con este. 

Primero, deberemos descargar lo siguiente:
- Una imagen de Raspbian. Utilizaremos la versión [Jesse](http://downloads.raspberrypi.org/raspbian/images/raspbian-2017-04-10/) en este tutorial. 
- Una imagen del kernel de QEMU de la misma versión que Raspbian, que se puede encontrar en el siguiente [repositorio](https://github.com/dhruvvyas90/qemu-rpi-kernel).
- Instala con el manejador de paquetes de tu distribución QEMU, ``qemu-system``. 

Una vez que tengas la imagen de Raspbian (descomprímela si es necesario), ejecuta el siguiente comando:
```
fdisk -l <version>.img
```
La salida mostrará algo parecido a lo siguiente:
```
Disk 2017-04-10-raspbian-jessie.img: 3.99 GiB, 4285005824 bytes, 8369152 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x402e4a57

Device                          Boot Start     End Sectors  Size Id Type
2017-04-10-raspbian-jessie.img1       8192   92159   83968   41M  c W95 FAT32 (LBA)
2017-04-10-raspbian-jessie.img2      92160 8369151 8276992  3.9G 83 Linux
```

Toma el sector de inicio (Start) de la segunda imagen (.img2) y multiplícalo por 512. En nuestro ejemplo, ``92160*512 = 47185920``. Este valor nos servirá como offset más adelante.

Crea un directorio para raspbian en ``/mnt/``:
```
sudo mkdir /mnt/raspbian
```

Monta la imagen sobre sobre este directorio utilizando el offset obtenido anteriormente, de la siguiente forma:
```
sudo mount -v -o offset=47185920 -t ext4 ~/qemu_vms/<your-img-file.img> /mnt/raspbian
```

Abre el archivo ``/mnt/raspbian/etc/ld.so.preload`` con un editor de texto y comenta todas sus lineas con ``#``. 

Abre el archivo ``/mnt/raspbian/etc/fstab``. Si vees algo que diga ``mmcblk0``:
- Reemplaza la primera entrada que contiene ``/dev/mmcblk0p1`` con ``/dev/sda1``.
- Reemplaza la segunda entrada que contiene ``/dev/mmcblk0p2`` con ``/dev/sda2``.

Desmonta el disco.
```
sudo umount /mnt/raspbian
```

Finalmente, para arrancar el emulador de Raspbian, ejecuta el siguiente comando.
```
qemu-system-arm \
  -kernel kernel-qemu-<version del kernel> \
  -hda <imagen de Raspbian>.img \
  -cpu arm1176 -m 256 \
  -M versatilepb \
  -serial stdio \
  -append "root=/dev/sda2 rootfstype=ext4 rw" \
  -net user,hostfwd=tcp::5022-:22 \
  -net nic
```

Una vez que haya arrancado Raspbian, abre una consola dentro del emulador y habilita el servicio de SSH.

![imagen](/images/blog/skidbladnir/tutorial_01.png)

Ahora desde tu host puedes conectarte por SSH a tu laboratorio de ARM utilizando el puerto que definimos en el comando de QEMU, ``5022``.
```
ssh pi@localhost -p 5022
```

Recuerda que en Raspbian la contraseña por default para el usuario ``pi`` es ``raspberry``. 

¡Listo! Has configurado tu laboratorio ARM. 

Sin embargo, una desventaja es que esta imagen de Raspbian por default tiene un tamaño de almacenamiento relativamente bajo. Esto traerá problemas si se intenta actualizar repositorios, o cargar programas de más de un par de cientos de MBs a la máquina virtual. 

## Para incrementar el almacenamiento virtual de tu laboratorio...

Crea una copia de la imagen existente.

```
cp <imagen original de Raspbian>.img  raspbian-copia-extendida.img
```

Incrementa el almacenamiento de la copia. En el siguiente ejemplo se muestra como incrementarla en 8 GB.

```
qemu-img resize raspbian-copia-extendida.img +8G
```

Arranca la máquina virtual utilizando la copia extendida como una segunda unidad de almacenamiento (el parámetro ``-hdb``).

```
qemu-system-arm \
  -kernel kernel-qemu-<version del kernel> \
  -hda <imagen original de Raspbian>.img \
  -hdb raspbian-copia-extendida.img
  -cpu arm1176 -m 256 \
  -M versatilepb \
  -serial stdio \
  -append "root=/dev/sda2 rootfstype=ext4 rw" \
  -net user,hostfwd=tcp::5022-:22 \
  -net nic
```

Ejecuta el siguiente comando en la terminal.

```
sudo cfdisk /dev/sdb
```

Se te mostrará un menú con las particiones del segundo disco. Elimina la partición sdb2 y crea una nueva partición (``New``) con todo el espacio disponible. Una vez que se cree la nueva partición, guarda los cambios (``Write``) y sal del programa (``Quit``). 

Ejecuta los siguientes comandso para ajustar el tamaño de la partición en el sistema de archivos.

```
sudo resize2fs /dev/sdb2
sudo fsck -f /dev/sdb2
```

Apaga la máquina virtual.

Ahora puedes volver a arrancar la máquina virtual, utilizando únicamente la imagen extendida como ``hda``.  

```
qemu-system-arm \
  -kernel kernel-qemu-<version del kernel> \
  -hda raspbian-copia-extendida.img
  -cpu arm1176 -m 256 \
  -M versatilepb \
  -serial stdio \
  -append "root=/dev/sda2 rootfstype=ext4 rw" \
  -net user,hostfwd=tcp::5022-:22 \
  -net nic
```